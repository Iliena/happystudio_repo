﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Actions;
namespace HappyStudioTesting_v2.Controls
{
    class AllActions
    {
        private static NavigationActions _navigationAction;
        public static NavigationActions  NavAction
        {
            get
            { return _navigationAction ?? (_navigationAction = new NavigationActions()); }
        }
    }
}
