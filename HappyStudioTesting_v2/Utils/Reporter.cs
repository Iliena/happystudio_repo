﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyStudioTesting_v2.Utils
{
    public static class Reporter
    {
        public static void Log (string msg)
        {
            Debug.WriteLine("[{0:T}] {1}", DateTime.Now, msg);
        }
    }
}
