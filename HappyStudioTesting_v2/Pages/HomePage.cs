﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Base;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Utils;
namespace HappyStudioTesting_v2.Pages
{
    class HomePage : BasePage
    {
        private By _mainLogo = By.XPath("//li[@class='name']//img");
        private By _mainBanner = By.XPath("//ul[@id='homepageSlider']/li[contains(@class, 'banner') and contains(@class, 'active')]//img");
        private By _navigationHolder = By.XPath("//div[@class='navHolder']");
        private By _parentPanelPromo = By.Id("parentPanelPromo");
        public void WaitForPageLoad ()
        {
            Reporter.Log("Go to home page");
            WaitForElementPresent(_navigationHolder);
            //WaitForInvisibility
            //class ExpectedCondition.IsElementClick (is enabled, displayed, stable)
        }

        public bool IsMainLogoPresent()
        {
            //WaitForElementPresent(BasePage.TimeoutSeconds, MainLogo);
            return IsElementPresent(MicroTimeoutSeconds,_mainLogo);
        }
        public bool IsMainBannerPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds, _mainBanner);
        }
        public bool IsNavigationOnBannerPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds, _navigationHolder);
        }
        public bool IsParentPanelPromoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds, _parentPanelPromo);
        }
    }
}
