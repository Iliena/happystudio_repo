﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;
namespace HappyStudioTesting_v2.Pages
{
    class BooksPage : BasePage
    {
        public static By MainLogo = By.XPath("//li[@class='name']//img");
        public static By MainBanner = By.XPath("//div[@class='books-banner']");
        public static By RightPromo = By.XPath("//div[@id='childBooks']");
        public static By Content = By.XPath("//div[@class='booksIntro-sections']");
        public void WaitForPageLoad()
        {
            Reporter.Log("Go to home page");
            WaitForElementPresent(MainBanner);
        }

        public bool IsMainLogoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainLogo);
        }
        public bool IsMainBannerPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainBanner);
        }
        public bool IsRightPromoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,RightPromo);
        }
        public bool IsContentPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,Content);
        }
    }
}
