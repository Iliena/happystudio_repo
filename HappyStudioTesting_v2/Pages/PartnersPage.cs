﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;
namespace HappyStudioTesting_v2.Pages
{
    class PartnersPage : BasePage
    {
        private By _mainLogo = By.XPath("//li[@class='name']//img");
        private By _mediaSmart = By.XPath("//section[@id='PartnersWrapper']//img[(contains(@src, 'media-smart.jpg'))]");
        private By _truste = By.XPath("//section[@id='PartnersWrapper']//img[(contains(@src, 'truste.jpg'))]");

        public void WaitForPageLoad()
        {
            Reporter.Log("Go to Partners page");
            WaitForElementPresent(_mediaSmart);
        }
        public bool IsMainLogoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,_mainLogo);
        }
        public bool IsMediaSmartPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,_mediaSmart);
        }
        public bool IsTrustePresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,_truste);
        }
    }
}
