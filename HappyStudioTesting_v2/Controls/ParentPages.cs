﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Pages;

namespace HappyStudioTesting_v2.Controls
{
    class ParentPages
    {        
        private static TopMenu _topMenu;
        private static HomePage _homePage;
        private static DiscoverPage _discoverPage;
        private static BooksPage _booksPage;
        private static CommitmentsPage _commitmentPage;
        private static PartnersPage _partnerPage;
        private static ControlPanel _controlPanel;
        public static HomePage HomePage
        {
            get { return _homePage ?? (_homePage = new HomePage()); }
        }
        public static DiscoverPage DiscoverPage
        {
            get { return _discoverPage ?? (_discoverPage = new DiscoverPage()); }
        }
        public static TopMenu TopMenu
        {
            get { return _topMenu ?? (_topMenu = new TopMenu()); }
        }
        public static BooksPage BooksPage
        {
            get { return _booksPage ?? (_booksPage = new BooksPage()); }
        }
        public static CommitmentsPage CommitPage
        {
            get { return _commitmentPage ?? (_commitmentPage = new CommitmentsPage()); }
        }
        public static PartnersPage PartnersPage
        {
            get { return _partnerPage ?? (_partnerPage = new PartnersPage()); }
        }
        public static ControlPanel ControlPanel
        {
            get { return _controlPanel ?? (_controlPanel = new ControlPanel()); }
        }
    }
}
