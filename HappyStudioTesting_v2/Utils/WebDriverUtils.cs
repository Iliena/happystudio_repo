﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System.Drawing;
using OpenQA.Selenium.Remote;

namespace HappyStudioTesting_v2.Utils
{
    public enum DriverType { InternetExplorer, Chrome, Firefox };

    class WebDriverUtils
    {
        public static RemoteWebDriver InitDriver(DriverType type, TimeSpan timeout)
        {
            Console.WriteLine("Initializing '{0}' driver...", type);
            RemoteWebDriver driver = null;
            switch (type)
            {
                case DriverType.Chrome:
                    ChromeOptions chOptions = new ChromeOptions();

                    // To remove message "You are using an unsupported command-line flag: --ignore-certificate-errors
                    // Stability and security will suffer."
                    chOptions.AddArgument("test-type");

                    // Enables the use of NPAPI plugins.
                    chOptions.AddArgument("enable-npapi");
                    // Disables the bundled PPAPI version of Flash.
                    chOptions.AddArgument("disable-bundled-ppapi-flash");
                    // Prevents Chrome from requiring authorization to run certain widely installed but less commonly used plugins.
                    chOptions.AddArgument("always-authorize-plugins");
                    // Disables the Web Notification and the Push APIs.
                    chOptions.AddArgument("disable-notifications");

                    // More Chrome command line switches:
                    // http://peter.sh/experiments/chromium-command-line-switches/

                    driver = new ChromeDriver(ChromeDriverService.CreateDefaultService(),chOptions, timeout);

                    break;
                case DriverType.InternetExplorer:
                    var ieOptions = new InternetExplorerOptions()
                    {
                        IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                        EnsureCleanSession = true,
                        InitialBrowserUrl = "about:blank"
                    };
                    driver = new InternetExplorerDriver(InternetExplorerDriverService.CreateDefaultService(), ieOptions,timeout);
                    break;
                case DriverType.Firefox:
                    driver = new FirefoxDriver(new FirefoxBinary(), new FirefoxProfile(), timeout);
                    break;
            }
            return driver;
        }
    }
}
