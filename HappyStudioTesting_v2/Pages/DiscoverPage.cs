﻿using HappyStudioTesting_v2.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Utils;
namespace HappyStudioTesting_v2.Pages
{
    class DiscoverPage : BasePage
    {
        public static By MainLogo = By.XPath("//li[@class='name']//img");
        public static By MainBanner = By.XPath("//div[@class='orbit-container']");
        public static By NavigationHolder = By.XPath("//div[@class='navHolder']");
        public static By Video = By.XPath("//div[@id='featureVideo']");


        public void WaitForPageLoad()
        {
            Reporter.Log("Go to Discover page");
            WaitForElementPresent(NavigationHolder);
        }

        public bool IsMainLogoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainLogo);
        }
        public bool IsMainBannerPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainBanner);
        }
        public bool IsNavigationOnBannerPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,NavigationHolder);
        }
        public bool IsVideoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,Video);
        }
    }
}
