﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Extensions;
namespace HappyStudioTesting_v2.Pages
{
    public class TopMenu : BasePage
    {
        private By _homeElement = By.XPath("//li[@id='navHome']/a");
        private By _discoverElement = By.XPath("//li[@id='navDiscover']/a");
        private By _booksElement = By.XPath("//li[@id='navBooks']/a");
        private By _commitmentElement = By.XPath("//li[@id='navCommitment']/a");
        private By _partnersElement = By.XPath("//li[@id='navPartners']/a");
        private By _dashboardElement = By.XPath("//li[@id='navDashboard']/a");
        private By _registerButton = By.XPath("//a[@class='txt-btn' and contains(@href, 'Register')]");

        public void ClickHome()
        {
            Click("Clicking Home button", _homeElement);
        }
        public void ClickDiscover()
        {
            Click("Clicking Discover button", _discoverElement);
        }
        public void ClickBooks()
        {
            Click("Clicking Books button", _booksElement);
        }
        public void ClickCommitment()
        {
            Click("Clicking Books button", _commitmentElement);
        }
        public void ClickPartners()
        {
            Click("Clicking Books button", _partnersElement);
        }
        public void ClickControlPanel()
        {
            Click("Clicking Books button", _dashboardElement);
        }

        public void ClickRegisterButton()
        {
            Click("Clicking Register Button", _registerButton);
        }
    }
}
