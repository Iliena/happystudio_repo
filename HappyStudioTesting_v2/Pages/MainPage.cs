﻿using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyStudioTesting_v2.Pages
{
    class MainPage : BasePage
    {
        public static By LanguageDialog = By.XPath("//div[@id='dialog-countrySelect']");
        public static By LanguageSelection = By.XPath("//a[(contains (@href, 'short_code=ru&country_code=ru'))]");
        public static By MainBanner = By.XPath("//div[@id='overlayContainer']");
        public static By ParentButton = By.XPath("//a[@id='parentsBtn']//div[@class='footerBtnCenter']");

        public void OpenMainPage ()
        {
            string url = ConfigurationManager.AppSettings["website"];
            BaseTest.Driver.Navigate().GoToUrl(url);
        }
        public void WaitForSelectionLanguageLoad()
        {
            Reporter.Log("Waiting for language selection is loaded");
            WaitForElementPresent(LanguageDialog);
        }
        public void WaitForPageLoad ()
        {
            Reporter.Log("Waiting for main page is loaded");
            WaitForElementPresent(ParentButton);
        }
        //for future modifications
        public void ChooseLanguage(string language="")
        {
            Reporter.Log("Choosing language on main page");
            if (language == "")
                Click("Choosing language", LanguageSelection);
        }
        public void ClickParentHome()
        {
            Reporter.Log("Clicking 'Groun ups' Home button");
            Click("Cliking parent home button", ParentButton);
        }
    }
}
