﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;
namespace HappyStudioTesting_v2.Pages
{
    class ControlPanel :BasePage
    {
        public static By MainLogo = By.XPath("//li[@class='name']//img");
        public static By BlockOfGames = By.XPath("//div[@class='BlockOfGame6']");
        public void WaitForPageLoad()
        {
            Reporter.Log("Go to Control panel page");
            WaitForElementPresent(BlockOfGames);
        }
        public bool IsMainLogoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainLogo);
        }
        public bool IsBlockOfGamesPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,BlockOfGames);
        }
    }
}
