﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using HappyStudioTesting_v2.Utils;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Pages;
using HappyStudioTesting_v2.Controls;
namespace HappyStudioTesting_v2.Tests
{
//    [TestFixture(DriverType.Chrome)]
    [TestFixture(DriverType.Firefox)]
    class HappyStudioNavigationTest : BaseTest
    {
        public HappyStudioNavigationTest(DriverType type) : base(type) { }

        [Test, Description("Testing Home Page is loading correctly")]
        public void TestHomePage()
        {
            HomePage home = ParentPages.HomePage;
            Assert.True(home.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(home.IsMainBannerPresent(), "Main banner seems to be missing");
            Assert.True(home.IsNavigationOnBannerPresent(), "Navigation on main banner seems to be missing");
            Assert.True(home.IsParentPanelPromoPresent(), "Parent panel seems to be missing");
        }

        [Test, Description("Testing Discover Page is loading correctly")]
        public void TestDiscoverPage()
        {
            AllActions.NavAction.OpenDiscoverPage();
            DiscoverPage discover = ParentPages.DiscoverPage;
            discover.WaitForPageLoad();
            Assert.True(discover.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(discover.IsMainBannerPresent(), "Main banner seems to be missing");
            Assert.True(discover.IsNavigationOnBannerPresent(), "Navigation on main banner seems to be missing");
            Assert.True(discover.IsVideoPresent(), "A video element seems to be missing");
        }

        [Test, Description("Testing Books Page is loading correctly"), Ignore ("No Books page on the page in English")]
        public void TestBooksPage()
        {
            AllActions.NavAction.OpenBooksPage();
            BooksPage books = ParentPages.BooksPage;
            books.WaitForPageLoad();
            Assert.True(books.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(books.IsMainBannerPresent(), "Main banner seems to be missing");
            Assert.True(books.IsRightPromoPresent(), "Right Promo element seems to be missing");
            Assert.True(books.IsContentPresent(), "Content of the page seems to be missing");
        }

        [Test, Description("Testing Commitment Page is loading correctly")]
        public void TestCommitmentPage()
        {
            AllActions.NavAction.OpenCommitmentPage();
            CommitmentsPage commit = ParentPages.CommitPage;
            commit.WaitForPageLoad();
            Assert.True(commit.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(commit.IsExpertsContentPresent(), "Part of the content 'Experts' seems to be missing");
            Assert.True(commit.IsDashBoardContentPresent(), "Part of the content 'Dashboard' seems to be missing");
            Assert.True(commit.IsFamilyContentPresent(), "Part of the content 'Family' seems to be missing");
            Assert.True(commit.IsReadingContentPresent(), "Part of the content 'Reading' seems to be missing");
        }

        [Test, Description("Testing Partners Page is loading correctly")]
        public void TestPartnersPage()
        {
            AllActions.NavAction.OpenPartnersPage();
            PartnersPage partners = ParentPages.PartnersPage;
            partners.WaitForPageLoad();
            Assert.True(partners.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(partners.IsMediaSmartPresent(), "Part of the content 'Media Smart' seems to be missing");
            Assert.True(partners.IsTrustePresent(), "Part of the content 'Truste' seems to be missing");
        }

        [Test, Description("Testing Control Panel Page is loading correctly")]
        public void TestControlPanelPage()
        {
            AllActions.NavAction.OpenControlPage();
            ControlPanel control = ParentPages.ControlPanel;
            control.WaitForPageLoad();
            Assert.True(control.IsMainLogoPresent(), "Main logo seems to be missing");
            Assert.True(control.IsBlockOfGamesPresent(), "Games content seems to be missing");
        }


        [Test, Description("Testing click on the top menu")]
        public void TopMenuClicks()
        {
            TopMenu top = ParentPages.TopMenu;
            top.ClickDiscover();
            //top.ClickBooks();
            top.ClickCommitment();
            top.ClickPartners();
            top.ClickControlPanel();
            top.ClickHome();
        }
    }
}
