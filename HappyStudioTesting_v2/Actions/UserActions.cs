﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Controls;
using HappyStudioTesting_v2.Utils;

namespace HappyStudioTesting_v2.Actions
{
    class UserActions
    {
        public void Register()
        {
            Reporter.Log("Trying to register new user");
            ParentPages.TopMenu.ClickRegisterButton(); 

        }
    }
}
