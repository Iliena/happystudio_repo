﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HappyStudioTesting_v2.Controls;
using HappyStudioTesting_v2.Utils;
using HappyStudioTesting_v2.Base;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Extensions;
namespace HappyStudioTesting_v2.Actions
{
    class NavigationActions
    {
        public void GoToStartPage ()
        {
            ExtraPages.MainPage.OpenMainPage();
            ExtraPages.MainPage.WaitForSelectionLanguageLoad();
            ExtraPages.MainPage.ChooseLanguage();
            ExtraPages.MainPage.WaitForPageLoad();
            ExtraPages.MainPage.ClickParentHome();
            ParentPages.HomePage.WaitForPageLoad();
        }
        public void OpenHomePage ()
        {
            //
            Reporter.Log("Opening home page from top menu");
            ParentPages.TopMenu.ClickHome();
            ParentPages.HomePage.WaitForPageLoad();
        }
        public void OpenDiscoverPage()
        {
            Reporter.Log("Open Discover page from top menu");
            ParentPages.TopMenu.ClickDiscover();
            ParentPages.DiscoverPage.WaitForPageLoad();
        }
        public void OpenBooksPage()
        {
            Reporter.Log("Open Books page from top menu");
            ParentPages.TopMenu.ClickBooks();
            ParentPages.BooksPage.WaitForPageLoad();
        }
        public void OpenCommitmentPage()
        {
            Reporter.Log("Open Commitments page from top menu");
            ParentPages.TopMenu.ClickCommitment();
            ParentPages.CommitPage.WaitForPageLoad();
        }
        public void OpenPartnersPage()
        {
            Reporter.Log("Open Partners page from top menu");
            ParentPages.TopMenu.ClickPartners();
            ParentPages.PartnersPage.WaitForPageLoad();
        }
        public void OpenControlPage()
        {
            Reporter.Log("Open Control page from top menu");
            ParentPages.TopMenu.ClickControlPanel();
            ParentPages.ControlPanel.WaitForPageLoad();
        }
    }
}
