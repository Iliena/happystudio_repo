﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;

namespace HappyStudioTesting_v2.Pages
{
    class CommitmentsPage : BasePage
    {
        public static By MainLogo = By.XPath("//li[@class='name']//img");
        public static By ExpertsContent = By.XPath("//div[@id='oc-expert']");
        public static By DashBoardContent = By.XPath("//div[@id='oc-dashboard']");
        public static By FamilyContent = By.XPath("//div[@id='oc-family']");
        public static By ReadingContent = By.XPath("//div[@id='oc-reading']");
        public void WaitForPageLoad()
        {
            Reporter.Log("Go to Commitments page");
            WaitForElementPresent(ExpertsContent);
        }
        public bool IsMainLogoPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,MainLogo);
        }
        public bool IsExpertsContentPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,ExpertsContent);
        }
        public bool IsDashBoardContentPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,DashBoardContent);
        }
        public bool IsFamilyContentPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,FamilyContent);
        }
        public bool IsReadingContentPresent()
        {
            return IsElementPresent(MicroTimeoutSeconds,ReadingContent);
        }
    }
}
