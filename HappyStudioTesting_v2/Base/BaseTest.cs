﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using HappyStudioTesting_v2.Utils;
using OpenQA.Selenium;
using HappyStudioTesting_v2.Controls;
using System.Configuration;

namespace HappyStudioTesting_v2.Base
{
    [TestFixture]
    class BaseTest
    {
        public static RemoteWebDriver Driver;

        public BaseTest(DriverType type)
        {
            int timeout = 7;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["maxTimeoutMin"], out timeout))
                Reporter.Log("Unable to set Max timeout. Check app.config.");
            Reporter.Log(String.Format("Setting max timeout to {0} minutes", timeout));
            Driver = WebDriverUtils.InitDriver(type, TimeSpan.FromMinutes(7));
            Driver.Manage().Window.Maximize();
            AllActions.NavAction.GoToStartPage();
        }
        [TestFixtureSetUp]
        public void SetUp()
        {
            Driver.Manage().Window.Maximize();
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
    }
}
