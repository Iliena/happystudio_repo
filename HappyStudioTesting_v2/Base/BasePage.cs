﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Utils;
using HappyStudioTesting_v2.Extensions;
namespace HappyStudioTesting_v2.Base
{
    public class BasePage
    {
        #region Timeout Constants
        public const int TimeoutSeconds = 60;
        public const int NormalTimeoutSeconds = 30;
        public const int SmallTimeoutSeconds = 15;
        public const int ExtrasmallTimeoutSeconds = 5;
        public const int MicroTimeoutSeconds = 2;
        #endregion

        private IWebDriver _driver;
        public IWebDriver Driver
        {
            get { return _driver ?? (_driver = BaseTest.Driver); }
        }
        protected void WaitForElementPresent(By element)
        {
            WaitForElementPresent(TimeoutSeconds, element);
        }

        protected void WaitForElementPresent(int timeout, By element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeout));
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));
            wait.Until(ExpectedConditions.ElementExists(element));
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(TimeoutSeconds));
        }
        protected void WaitForElementVisible(By element)
        {
            WaitForElementVisible(TimeoutSeconds, element);
        }
        protected void WaitForElementVisible(int timeout, By element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeout));
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));
            wait.Until(ExpectedConditions.ElementIsVisible(element));
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(TimeoutSeconds));
        }
        protected void WaitForElementInvisible(By element)
        {
            WaitForElementInvisible(TimeoutSeconds, element);
        }
        protected void WaitForElementInvisible(int timeout, By element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeout));
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));
            wait.Until(MyExpectedCondition.IsElementInvisible(element));
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(TimeoutSeconds));
        }
        protected void WaitForElementClickable (By element)
        {
            WaitForElementClickable(TimeoutSeconds, element);
        }
        protected void WaitForElementClickable(int timeout,By element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeout));
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));
            wait.Until(MyExpectedCondition.IsElementClickable(element));
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(TimeoutSeconds));

        }
        public bool IsElementPresent(By locator)
        {
            return Driver.FindElements(locator).Any();
        }
        public bool IsElementPresent(int timeout, By locator)
        {
            return GetElementsCount(timeout, locator) > 0;
        }
        protected void Click (string msg, By element)
        {
            Reporter.Log(msg);
            GetElement(element).Click();
        }
        protected int GetElementsCount(By element)
        {
            return GetElementsCount(0, element);
        }
        protected int GetElementsCount(int waitInSeconds, By element)
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(waitInSeconds));
            int size = GetElements(element).Count;
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(TimeoutSeconds));
            return size;
        }
        protected void Type(string message, string value, By element)
        {
            IWebElement input = GetElement(element);
            input.Clear();
            if (!string.IsNullOrEmpty(value))
            {
                input.SendKeys(value);
            }
        }
        protected void TypeWithoutClear(string message, string value, By element)
        {
            GetElement(element).SendKeys(value);
        }
        protected IWebElement GetElement(By element)
        {
            return Driver.FindElement(element);
        }
        protected List<IWebElement> GetElements(By element)
        {
            return Driver.FindElements(element).ToList();
        }

    }
}
