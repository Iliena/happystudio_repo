﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HappyStudioTesting_v2.Base;
using HappyStudioTesting_v2.Utils;
using OpenQA.Selenium;

namespace HappyStudioTesting_v2.Pages
{
    class RegisterPage : BasePage
    {
        private By _registrationForm = By.XPath("//form[@id='registerform]");
        private By _inputFirstName = By.Id("firstname-preregister");
        private By _inputLastName = By.Id("lastname-preregister");
        private By _inputEmail = By.Id("email");
        private By _inputPass = By.Id("acode-preregister");
        private By _submitButton = By.XPath("//button[@type='submit']");
        public void WaitForPageLoad()
        {
            Reporter.Log("Go to registration page");
            WaitForElementPresent(_registrationForm);
        }
        public void ClickSubmit()
        {
            Click("Clicking Submit button on Register page", _submitButton);
        }
        public void FillRegisterForm(string firstName, string lastName, string email, string pass)
        {
            Type("Typing first name", firstName, _inputFirstName);
            Type("Typing last name", lastName, _inputLastName);
            Type("Typing email", email, _inputEmail);
            Type("Typing password", pass, _inputPass);
        }

    }
}
