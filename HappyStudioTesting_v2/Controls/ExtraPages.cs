﻿using HappyStudioTesting_v2.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyStudioTesting_v2.Controls
{
    class ExtraPages
    {
        private static MainPage _mainPage;
        private static RegisterPage _registerPage;
        public static MainPage MainPage
        {
            get { return _mainPage ?? (_mainPage = new MainPage()); }
        }

        public static RegisterPage RegisterPage
        {
            get { return _registerPage ?? (_registerPage = new RegisterPage()); }
        }
    }
}
