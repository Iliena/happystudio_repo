﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace HappyStudioTesting_v2.Extensions
{
    public static class MyExpectedCondition
    {
        public static Func<IWebDriver, IWebElement> IsElementClickable(By locator)
        {
            return (driver) =>
            {
                IWebElement element = driver.FindElement(locator);
                try
                {
                    if (element != null && element.Enabled && element.Displayed)
                    {
                        Point startpoint = element.Location;
                        Size startSize = element.Size;
                        System.Threading.Thread.Sleep(500);

                        if (startpoint.Equals(element.Location) && startSize.Equals(element.Size))
                            return element;
             
                    }
                    return null;
                }
                catch (NoSuchElementException)
                {
                    return null;
                }
                catch (StaleElementReferenceException)
                {
                    return null;
                }
            };
        }
        public static Func<IWebDriver, bool> IsElementInvisible(By locator)
        {
            return (driver) =>
            {
                try
                {
                    IWebElement element = driver.FindElement(locator);
                    if (!element.Displayed && element.Size.Height >0 && element.Size.Width >0)
                        return true;
                    return false;
                }
                catch (NoSuchElementException)
                {
                    return true;
                }
                catch (StaleElementReferenceException)
                {
                    return false;
                }
            };
        }
    }
}
